﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.ViewModels.House
{
    public class EditHouseSpec
    {
        public string Name { get; set; }

        public string Address { get; set; }
    }
}
