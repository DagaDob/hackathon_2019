﻿using ShoppingListPro.Data;
using ShoppingListPro.Data.Tables;
using ShoppingListPro.ViewModels.ShoppingList;
using ShoppingListPro.ViewModels.UserVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.ViewModels.House
{
    public class HouseDetailsVM : HouseShortDetailsVM
    {
        public List<ShoppingListItemVM> ShoppingList { get; set; }

        public List<UserShortDetailsVM> Users { get; set; }
    }
}
