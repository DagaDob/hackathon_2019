﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.ViewModels.UserVM
{
    public class UserDetailsVM : UserShortDetailsVM
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public DateTime RegisterDate { get; set; }

        public DateTime? BirthDate { get; set; }
    }
}
