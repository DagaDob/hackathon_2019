﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.ViewModels.UserVM
{
    public class NewUserSpec
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string RepeatPassword { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }


        //albo to - przypisac do istniejacego domu
        public int? HouseId { get; set; }


        //albo podac dane noego domu (nazwa i adres)
        public string HouseName { get; set; }

        public string HouseAddress { get; set; }
    }
}
