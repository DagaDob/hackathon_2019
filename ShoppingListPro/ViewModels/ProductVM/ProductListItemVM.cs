﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.ViewModels.ProductVM
{
    public class ProductListItemVM
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Cathegory { get; set; }

        public string ImageUrl { get; set; }

        public string ShopAddress { get; set; }

        public string HouseName { get; set; }
    }
}
