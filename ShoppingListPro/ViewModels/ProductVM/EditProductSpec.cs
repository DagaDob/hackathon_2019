﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.ViewModels.ProductVM
{
    public class EditProductSpec : CreateProductSpec
    {
        [Required]
        public int Id { get; set; }
    }
}
