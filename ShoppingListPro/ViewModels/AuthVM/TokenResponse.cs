﻿using ShoppingListPro.ViewModels.UserVM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.ViewModels.AuthVM
{
    public class TokenResponse
    {
        [Required]
        public string Token { get; set; }

        [Required]
        public string RefreshToken { get; set; }

        [Required]
        public UserDetailsVM User { get; set; }
    }
}
