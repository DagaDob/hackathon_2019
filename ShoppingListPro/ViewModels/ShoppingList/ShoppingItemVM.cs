﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.ViewModels.ShoppingList
{
    public class ShoppingItemVM : ShoppingListItemVM
    {
        public string Category { get; set; }

        public string ShopAddress { get; set; }

        public string ImageUrl { get; set; }

        public string CreatorId { get; set; }

        public string CreatorName { get; set; }

        public int HouseId { get; set; }
    }
}
