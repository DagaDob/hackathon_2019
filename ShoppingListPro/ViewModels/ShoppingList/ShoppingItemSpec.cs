﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.ViewModels.ShoppingList
{
    public class ShoppingItemSpec
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public int ProductId { get; set; }

        public string Amount { get; set; }

        public string Unit { get; set; }
    }
}
