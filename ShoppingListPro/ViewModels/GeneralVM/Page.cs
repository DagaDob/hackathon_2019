﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.ViewModels.GeneralVM
{
    public class Page<T>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int PageCount { get; set; }

        public List<T> Results { get; set; }


        public Page(int pageNumber, int pageSize, int pageCount, List<T> results)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            PageCount = pageCount;
            Results = results;
        }
    }
}
