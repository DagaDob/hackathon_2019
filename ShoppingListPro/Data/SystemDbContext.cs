﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ShoppingListPro.Data.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Data
{
    public class SystemDbContext : IdentityDbContext<ApplicationUser>
    {
        public SystemDbContext(DbContextOptions<SystemDbContext> options) : base(options) { }

        public DbSet<House> Houses { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ShoppingListItem> ShoppingListItems { get; set; }
        public DbSet<ProductCathegory> ProductCathegories { get; set; }


        public async Task<int> CountSoftDeletable<T>() where T : class, ISoftDeletable
        {
            return await Set<T>().Where(i => !i.IsDeleted)
                .CountAsync();
        }
    }
}
