﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Data.Tables
{
    public class Product : ISoftDeletable
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Cathegory { get; set; }

        public string ImageUrl { get; set; }

        public string ShopAddress { get; set; }

        public int? HouseId { get; set; }

        [ForeignKey("HouseId")]
        public virtual House House { get; set; }

        public bool IsDeleted { get; set; } = false;
    }
}
