﻿using Microsoft.AspNetCore.Identity;
using ShoppingListPro.Data.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Data
{
    public class ApplicationUser : IdentityUser, ISoftDeletable
    {
        [Required]
        public DateTime RegisterDate { get; set; }

        public string RefreshToken { get; set; }  //wystawiany po logowaniu

        public DateTime? BirthDate { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public bool IsDeleted { get; set; } = false;

        public int? HouseId { get; set; }

        [ForeignKey("HouseId")]
        public virtual House House { get; set; }
    }
}
