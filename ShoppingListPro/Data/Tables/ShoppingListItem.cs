﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Data.Tables
{
    public class ShoppingListItem : ISoftDeletable
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

        public string Amount { get; set; }

        public string Unit { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser Creator { get; set; }

        public int HouseId { get; set; }
        [ForeignKey("HouseId")]
        public virtual House House { get; set; }

        public bool IsDeleted { get; set; } = false;
    }
}
