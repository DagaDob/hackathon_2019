﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Data.Tables
{
    public class House : ISoftDeletable
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public virtual ICollection<ShoppingListItem> ShoppingListItem { get; set; } = new HashSet<ShoppingListItem>();

        public virtual ICollection<ApplicationUser> Users { get; set; } = new HashSet<ApplicationUser>();

        public bool IsDeleted { get; set; } = false;
    }
}
