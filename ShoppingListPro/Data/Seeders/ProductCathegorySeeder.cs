﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Data.Seeders
{
    public class ProductCathegorySeeder
    {
        public ProductCathegorySeeder(SystemDbContext context)
        {
            Context = context;
        }

        public SystemDbContext Context { get; }

        public void Seed()
        {
            if (Context.ProductCathegories.Count() == 0)
            {
                foreach (var row in Const.ProductCathegory.cathegories)
                {
                    if (!Context.ProductCathegories.Any(i => i.Name == row))
                    {
                        var item = new Tables.ProductCathegory()
                        {
                            Name = row
                        };

                        var result = Context.ProductCathegories.AddAsync(item).Result;
                    }
                }
                Context.SaveChanges();
            }
        }
    }
}
