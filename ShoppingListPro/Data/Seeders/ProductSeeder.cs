﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Data.Seeders
{
    public class ProductSeeder
    {
        public ProductSeeder(SystemDbContext context)
        {
            Context = context;
        }

        private SystemDbContext Context { get; }


        public void Seed()
        {
            if (Context.Products.Count() == 0)
            {
                var category = Const.ProductCathegory.cathegories[0];

                Context.Products.Add(new Tables.Product() { Name = "Banan", Cathegory = category, ShopAddress = "Mazowiecka 5" });
                Context.Products.Add(new Tables.Product() { Name = "Śliwka", Cathegory = category, ShopAddress = "Mazowiecka 5" });
                Context.Products.Add(new Tables.Product() { Name = "Gruszka", Cathegory = category, ShopAddress = "Mazowiecka 5" });

                category = Const.ProductCathegory.cathegories[1];

                Context.Products.Add(new Tables.Product() { Name = "Marchew", Cathegory = category, ShopAddress = "Mazowiecka 5" });
                Context.Products.Add(new Tables.Product() { Name = "Kapusta", Cathegory = category, ShopAddress = "Jaworzyńska 33/1" });
                Context.Products.Add(new Tables.Product() { Name = "Ziemniak", Cathegory = category, ShopAddress = "Jaworzyńska 33/1" });

                category = Const.ProductCathegory.cathegories[2];

                Context.Products.Add(new Tables.Product() { Name = "Wódka Soplica", Cathegory = category, ShopAddress = "Mazowiecka 5" });
                Context.Products.Add(new Tables.Product() { Name = "Lech (Puszka)", Cathegory = category, ShopAddress = "Jaworzyńska 33/1" });
                Context.Products.Add(new Tables.Product() { Name = "Lech (Butelka)", Cathegory = category, ShopAddress = "Jaworzyńska 33/1" });
                Context.Products.Add(new Tables.Product() { Name = "Jack Daniels", Cathegory = category, ShopAddress = "Jaworzyńska 33/1" });

                category = Const.ProductCathegory.cathegories[3];


                Context.Products.Add(new Tables.Product() { Name = "Karkówka", Cathegory = category, ShopAddress = "Mazowiecka 5" });
                Context.Products.Add(new Tables.Product() { Name = "Pierś z kurczaka", Cathegory = category, ShopAddress = "Jaworzyńska 33/1" });
                Context.Products.Add(new Tables.Product() { Name = "Mięso mielone", Cathegory = category, ShopAddress = "Złotoryjska 12" });
                Context.Products.Add(new Tables.Product() { Name = "Pierś z kaczki", Cathegory = category, ShopAddress = "Złotoryjska 12" });

                category = Const.ProductCathegory.cathegories[4];


                Context.Products.Add(new Tables.Product() { Name = "Czekolada gorzka 75%", Cathegory = category, ShopAddress = "Jaworzyńska 33/1" });
                Context.Products.Add(new Tables.Product() { Name = "Oreo", Cathegory = category, ShopAddress = "Złotoryjska 12" });
                Context.Products.Add(new Tables.Product() { Name = "Snickers", Cathegory = category, ShopAddress = "Złotoryjska 12" });

                category = Const.ProductCathegory.cathegories[5];

                Context.Products.Add(new Tables.Product() { Name = "Mleko", Cathegory = category, ShopAddress = "Mazowiecka 5" });
                Context.Products.Add(new Tables.Product() { Name = "Jajka ściółkowe L", Cathegory = category, ShopAddress = "Mazowiecka 5" });

                category = Const.ProductCathegory.cathegories[6];


                Context.Products.Add(new Tables.Product() { Name = "Ser biały", Cathegory = category, ShopAddress = "Jaworzyńska 33/1" });
                Context.Products.Add(new Tables.Product() { Name = "Ser żółty", Cathegory = category, ShopAddress = "Złotoryjska 12" });
                Context.Products.Add(new Tables.Product() { Name = "Ser pleśniowy", Cathegory = category, ShopAddress = "Mazowiecka 5" });

                category = Const.ProductCathegory.cathegories[7];

                Context.Products.Add(new Tables.Product() { Name = "Chleb wiejski", Cathegory = category, ShopAddress = "Jaworzyńska 33/1" });
                Context.Products.Add(new Tables.Product() { Name = "Kajzerka", Cathegory = category, ShopAddress = "Złotoryjska 12" });
                Context.Products.Add(new Tables.Product() { Name = "Rogalik", Cathegory = category, ShopAddress = "Mazowiecka 5" });

                Context.SaveChanges();
            }
        }
    }
}
