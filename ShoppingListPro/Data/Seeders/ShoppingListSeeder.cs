﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Data.Seeders
{
    public class ShoppingListSeeder
    {
        public ShoppingListSeeder(SystemDbContext context)
        {
            Context = context;
        }

        private SystemDbContext Context { get; }


        public void Seed()
        {
            if (Context.ShoppingListItems.Count() == 0)
            {
                Context.ShoppingListItems.Add(new Tables.ShoppingListItem()
                {
                    Name = "lista1",
                    ProductId = 1,
                    Amount = "10",
                    Unit = "szt.",
                    UserId = Context.Users.First(i => !i.IsDeleted).Id,
                    HouseId = Context.Houses.First(i => !i.IsDeleted).Id
                });

                Context.ShoppingListItems.Add(new Tables.ShoppingListItem()
                {
                    Name = "lista1",
                    ProductId = 2,
                    Amount = "1",
                    Unit = "szt.",
                    UserId = Context.Users.First(i => !i.IsDeleted).Id,
                    HouseId = Context.Houses.First(i => !i.IsDeleted).Id
                });

                Context.ShoppingListItems.Add(new Tables.ShoppingListItem()
                {
                    Name = "lista1",
                    ProductId = 3,
                    Amount = "500",
                    Unit = "g",
                    UserId = Context.Users.First(i => !i.IsDeleted).Id,
                    HouseId = Context.Houses.First(i => !i.IsDeleted).Id
                });

                Context.SaveChanges();
            }
        }
    }
}
