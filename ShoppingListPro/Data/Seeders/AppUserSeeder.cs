﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Data.Seeders
{
    public class AppUserSeeder
    {
        public AppUserSeeder(SystemDbContext context, UserManager<ApplicationUser> manager)
        {
            Context = context;
            Manager = manager;
        }

        private SystemDbContext Context { get; }
        private UserManager<ApplicationUser> Manager { get; }

        public void Seed()
        {
            var user = new ApplicationUser()
            {
                UserName = "admin@example.com",
                Email = "admin@example.com",
                BirthDate = new DateTime(1980, 08, 12),
                RegisterDate = new DateTime(2019, 01, 01),
                Name = "Mateusz",
                Surname = "Nowak",
                HouseId = Context.Houses.First(i => !i.IsDeleted).Id
            };
            if (Manager.FindByEmailAsync(user.Email).Result == null)
            {
                var result = Manager.CreateAsync(user, "1234").Result;
            }
        }
    }
}
