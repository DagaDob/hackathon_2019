﻿using ShoppingListPro.Data.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Data.Seeders
{
    public class HouseSeeder
    {
        public HouseSeeder(SystemDbContext context)
        {
            Context = context;
        }

        private SystemDbContext Context { get; }

        public void Seed()
        {
            if (Context.Houses.Count() == 0)
            {
                Context.Houses.Add(
                    new House()
                    {
                        Name = "Akademik",
                        Address = "ul. Mickiewicza 1, 59-220 Legnica",
                    }
                );

                Context.SaveChanges();


                Context.Houses.Add(
                    new House()
                    {
                        Name = "Dom",
                        Address = "ul. Kolbego 1, 59-220 Legnica",
                    }
                );

                Context.SaveChanges();
            }
        }
    }
}
