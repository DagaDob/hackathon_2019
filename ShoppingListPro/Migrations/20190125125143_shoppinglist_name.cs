﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ShoppingListPro.Migrations
{
    public partial class shoppinglist_name : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Name",
                table: "ShoppingListItems",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "ShoppingListItems");
        }
    }
}
