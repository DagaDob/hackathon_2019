﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ShoppingListPro.Migrations
{
    public partial class product_cathegory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Cathegory",
                table: "Products",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Cathegory",
                table: "Products");
        }
    }
}
