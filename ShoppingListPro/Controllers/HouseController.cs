﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NSwag.Annotations;
using ShoppingListPro.Data;
using ShoppingListPro.Data.Tables;
using ShoppingListPro.Services;
using ShoppingListPro.ViewModels.GeneralVM;
using ShoppingListPro.ViewModels.House;
using ShoppingListPro.ViewModels.ShoppingList;
using ShoppingListPro.ViewModels.UserVM;

namespace ShoppingListPro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class HouseController : BaseController
    {
        private ILogger<HouseController> Logger { get; }
        private IHouseService HouseService { get; }

        public HouseController(
            UserManager<ApplicationUser> userManager,
            ILoggerFactory loggerFactory,
            SystemDbContext context,
            IHouseService houseService
            ) : base(userManager, loggerFactory, context)
        {
            Logger = loggerFactory.CreateLogger<HouseController>();
            HouseService = houseService;
        }


        [HttpGet]
        [Authorize]
        [SwaggerResponse(StatusCodes.Status200OK, typeof(HouseDetailsVM))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status404NotFound, null)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> Get()
        {
            try
            {
                var loggedUser = await LoggedUserAsync();

                if (loggedUser == null)
                    return Unauthorized();

                var result = await HouseService.GetHouse(loggedUser);

                if(result == null)
                    return NotFound(ToErrorDict("error", "invalid data"));

                return Ok(result);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(Get)}()");
                return InternalServerError();
            }
        }

        [HttpGet("houses")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, typeof(IEnumerable<HouseDetailsVM>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status404NotFound, null)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> GetHouses([FromQuery] int page = 0, [FromQuery] int? pageSize = 10)
        {
            try
            {
                if (page < 0 || pageSize < 1)
                    return BadRequest(ToErrorDict("error", "invalid page parameters!"));
                
                var pageCountTask = HouseService.GetPageCount(pageSize.Value);
                var housesTask = HouseService.GetHouses(page, pageSize.Value);

                await Task.WhenAll(pageCountTask, housesTask);


                Page<HouseShortDetailsVM> pageResult = new Page<HouseShortDetailsVM>(
                    page,
                    pageCountTask.Result,
                    pageSize.Value,
                    housesTask.Result);

                return Ok(pageResult);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(GetHouses)}()");
                return InternalServerError();
            }
        }

        [HttpPut]
        [Authorize]
        [SwaggerResponse(StatusCodes.Status200OK, null)]
        [SwaggerResponse(StatusCodes.Status400BadRequest, typeof(Dictionary<string, string[]>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status404NotFound, typeof(Dictionary<string, string[]>))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> Put([FromBody] EditHouseSpec request)
        {
            try
            {
                var loggedUser = await LoggedUserAsync();
                if (loggedUser == null)
                    return Unauthorized();

                var result = await HouseService.EditHouse(request, loggedUser);

                if (!result)
                    return NotFound(ToErrorDict("error", "invalid data"));

                return Ok();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(Put)}()");
                return InternalServerError();
            }
        }
    }
}