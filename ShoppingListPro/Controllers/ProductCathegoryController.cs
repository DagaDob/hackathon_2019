﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSwag.Annotations;
using ShoppingListPro.Data;
using ShoppingListPro.Services;
using ShoppingListPro.ViewModels.ProductCathegoriesVM;

namespace ShoppingListPro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ProductCathegoryController : BaseController
    {
        private ILogger<ProductCathegoryController> Logger { get; }
        private IProductCathegoryService CathegoryService { get; }

        public ProductCathegoryController(
            UserManager<ApplicationUser> userManager,
            ILoggerFactory loggerFactory,
            SystemDbContext context, 
            IProductCathegoryService cathegoryService
            ) : base(userManager, loggerFactory, context)
        {
            Logger = loggerFactory.CreateLogger<ProductCathegoryController>();
            CathegoryService = cathegoryService;
        }

        [HttpGet]
        [Authorize]
        [SwaggerResponse(StatusCodes.Status200OK, typeof(IEnumerable<CathegoryVM>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status404NotFound, null)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> Get([FromQuery] int page = 0, [FromQuery] int? pageSize = 10)
        {
            try
            {
                var cathegories = await CathegoryService.GetCathegories(page, pageSize.Value);

                if (cathegories == null)
                    return NotFound();

                return Ok(cathegories);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "ERROR! Unexpected error in List()");
                return InternalServerError();
            }
        }


        [HttpGet("{id}")]
        [Authorize]
        [SwaggerResponse(StatusCodes.Status200OK, typeof(CathegoryVM))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, typeof(Dictionary<string, string[]>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status404NotFound, null)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> Get([FromRoute]int id)
        {
            try
            {
                var item = await CathegoryService.GetCathegoryType(id);

                if (item == null)
                    return NotFound();

                return Ok(item);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(Get)}() eventType");
                return InternalServerError();
            }
        }

        [HttpPost]
        [Authorize]
        [SwaggerResponse(StatusCodes.Status200OK, typeof(int))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, typeof(Dictionary<string, string[]>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> Post([FromBody]CreateCathegorySpec request)
        {
            try
            {
                var newCathegoryId = await CathegoryService.AddCathegoryType(request);
                
                return Ok(newCathegoryId);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(Post)}()");
                return InternalServerError();
            }
        }
    }
}