﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSwag.Annotations;
using ShoppingListPro.Data;
using ShoppingListPro.Services;
using ShoppingListPro.ViewModels.GeneralVM;
using ShoppingListPro.ViewModels.ProductVM;

namespace ShoppingListPro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ProductController : BaseController
    {
        private ILogger<ProductController> Logger { get; }
        private IProductService ProductService { get; }

        public ProductController(
            UserManager<ApplicationUser> userManager,
            ILoggerFactory loggerFactory,
            SystemDbContext context,
            IProductService productService
            ) : base(userManager, loggerFactory, context)
        {
            Logger = loggerFactory.CreateLogger<ProductController>();
            ProductService = productService;
        }
        
        
        [HttpGet]
        [Authorize]
        [SwaggerResponse(StatusCodes.Status200OK, typeof(IEnumerable<ProductListItemVM>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status404NotFound, null)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> Get([FromQuery] int page = 0, [FromQuery] int? pageSize = 10)
        {
            try
            {
                if (page < 0 || pageSize < 1)
                    return BadRequest(ToErrorDict("error", "invalid page parameters!"));

                var loggedUser = await LoggedUserAsync();

                var pageCountTask = ProductService.GetPageCount(pageSize.Value);
                var productsTask = ProductService.GetProducts(loggedUser, page, pageSize.Value);

                await Task.WhenAll(pageCountTask, productsTask);

                
                Page<ProductListItemVM> pageResult = new Page<ProductListItemVM>(
                    page,
                    pageCountTask.Result,
                    pageSize.Value,
                    productsTask.Result);

                return Ok(pageResult);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(Get)}()");
                return InternalServerError();
            }
        }

        [HttpPost]
        [Authorize]
        [SwaggerResponse(StatusCodes.Status200OK, typeof(int?))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, typeof(Dictionary<string, string[]>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> Post([FromBody] CreateProductSpec request)
        {
            try
            {
                var newProduct = await ProductService.AddProduct(request);

                if (newProduct == null)
                    return BadRequest(ToErrorDict("error", "Create product failed!"));

                return Ok(newProduct);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(Post)}() (create product)");
                return InternalServerError();
            }
        }

        [HttpPut]
        [SwaggerResponse(StatusCodes.Status200OK, null)]
        [SwaggerResponse(StatusCodes.Status400BadRequest, typeof(Dictionary<string, string[]>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status404NotFound, typeof(Dictionary<string, string[]>))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> Put([FromBody] EditProductSpec request)
        {
            try
            {
                var loggedUser = await LoggedUserAsync();

                var isEdited = await ProductService.EditProduct(request, loggedUser);

                if (!isEdited)
                    return BadRequest(ToErrorDict("error", "Edit failed!"));

                return Ok();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(Put)}() (edit product)");
                return InternalServerError();
            }
        }
    }
}