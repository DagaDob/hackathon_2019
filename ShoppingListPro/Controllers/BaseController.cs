﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShoppingListPro.Data;

namespace ShoppingListPro.Controllers
{
    public class BaseController : Controller
    {
        public BaseController(
            UserManager<ApplicationUser> userManager,
            ILoggerFactory loggerFactory,
            SystemDbContext context)
        {
            this.UserManager = userManager;
            this.LoggerFactory = loggerFactory;
            this.Context = context;
        }

        protected UserManager<ApplicationUser> UserManager { get; set; }
        protected ILoggerFactory LoggerFactory { get; }
        protected SystemDbContext Context { get; }



        protected StatusCodeResult InternalServerError()
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
        
        protected Dictionary<string, string[]> ToErrorDict(string fieldName, params string[] errors)
        {
            return new Dictionary<string, string[]>() { { fieldName, errors } };
        }

        protected async Task<ApplicationUser> LoggedUserAsync()
        {
            return await UserManager.FindByEmailAsync(HttpContext.User.Identity.Name);
        }
    }
}