﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NSwag.Annotations;
using ShoppingListPro.Data;
using ShoppingListPro.Services;
using ShoppingListPro.ViewModels.GeneralVM;
using ShoppingListPro.ViewModels.ShoppingList;

namespace ShoppingListPro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ShoppingController : BaseController
    {
        private ILogger<ShoppingController> Logger { get; }
        private IShoppingService ShoppingService { get; }

        public ShoppingController(
            UserManager<ApplicationUser> userManager, 
            ILoggerFactory loggerFactory, 
            SystemDbContext context,
            IShoppingService shoppingService
            ) : base(userManager, loggerFactory, context)
        {
            Logger = loggerFactory.CreateLogger<ShoppingController>();
            ShoppingService = shoppingService;
        }


        [HttpGet]
        [Authorize]
        [SwaggerResponse(StatusCodes.Status200OK, typeof(IEnumerable<ShoppingItemVM>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status404NotFound, null)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> Get([FromQuery] int page = 0, [FromQuery] int? pageSize = 10)
        {
            try
            {
                if (page < 0 || pageSize < 1)
                    return BadRequest(ToErrorDict("error", "invalid page parameters"));

                var loggedUser = await LoggedUserAsync();

                if (loggedUser == null)
                    return Unauthorized();

                if (loggedUser.HouseId == null)
                    return NotFound(ToErrorDict("error", "No house assigned to user"));

                var pageCountTask = ShoppingService.GetPageCount(pageSize.Value);
                var shoppingListTask = ShoppingService.GetShoppingList(loggedUser, page, pageSize.Value);

                await Task.WhenAll(pageCountTask, shoppingListTask);

                Page<ShoppingItemVM> pageResult = new Page<ShoppingItemVM>(
                    page,
                    pageCountTask.Result,
                    pageSize.Value,
                    shoppingListTask.Result);

                return Ok(pageResult);

            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(Get)}()");
                return InternalServerError();
            }
        }

        [Authorize]
        [HttpDelete("{id:int}")]
        [SwaggerResponse(StatusCodes.Status200OK, null)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status404NotFound, null)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var loggedUser = await LoggedUserAsync();
                if (loggedUser == null)
                    return Unauthorized();
                
                var isDeleted = ShoppingService.DeleteShoppingListItem(id, loggedUser);

                return Ok();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(Delete)}()");
                return InternalServerError();
            }
        }

        //add or edit shopping item
        [Authorize]
        [HttpPost]
        [SwaggerResponse(StatusCodes.Status200OK, null)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status404NotFound, null)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> Post([FromBody] ShoppingItemSpec request)
        {
            try
            {
                var loggedUser = await LoggedUserAsync();

                if (loggedUser == null)
                    return Unauthorized();

                if (loggedUser.HouseId == null)
                    return NotFound(ToErrorDict("error", "No house assigned to user"));

                var result = await ShoppingService.AddOrUpdateShoppingListItem(request, loggedUser);

                if(!result)
                    return NotFound(ToErrorDict("error", "invalid data"));

                return Ok();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(Post)}()");
                return InternalServerError();
            }
        }
    }
}