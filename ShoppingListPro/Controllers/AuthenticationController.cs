﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NSwag.Annotations;
using ShoppingListPro.Data;
using ShoppingListPro.Services;
using ShoppingListPro.ViewModels.AuthVM;
using ShoppingListPro.ViewModels.UserVM;

namespace ShoppingListPro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class AuthenticationController : BaseController
    {
        public AuthenticationController(
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager,
            ILoggerFactory loggerFactory,
            IConfiguration configuration,
            PasswordHasher<ApplicationUser> passwordHasher,
            ITokenService tokenService,
            SystemDbContext context)
            : base(userManager, loggerFactory, context)
        {
            Logger = loggerFactory.CreateLogger<AuthenticationController>();
            SignInManager = signInManager;
            Configuration = configuration;
            PasswordHasher = passwordHasher;
            TokenService = tokenService;
        }

        public SignInManager<ApplicationUser> SignInManager { get; }
        private IConfiguration Configuration { get; }
        private PasswordHasher<ApplicationUser> PasswordHasher { get; }
        private ITokenService TokenService { get; }
        private ILogger<AuthenticationController> Logger { get; set; }


        [HttpPost("login")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, typeof(TokenResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, typeof(Dictionary<string, string[]>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> LogIn([FromBody]SignInSpec logInRequest)
        {
            try
            {
                var user = await UserManager.FindByEmailAsync(logInRequest.Username);
                if (user == null)
                    return Unauthorized();

                var result = PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, logInRequest.Password);

                if (result == PasswordVerificationResult.Failed || result == PasswordVerificationResult.SuccessRehashNeeded)
                    return Unauthorized();
                
                var token = TokenService.GenerateJwtToken(user.Id, user.UserName);

                var refreshToken = TokenService.GenerateRefreshToken();

                user.RefreshToken = refreshToken;
                await UserManager.UpdateAsync(user);

                return Ok(
                    new TokenResponse()
                    {
                        Token = new JwtSecurityTokenHandler().WriteToken(token),
                        RefreshToken = refreshToken,
                        User = new UserDetailsVM()
                        {
                            Id = user.Id,
                            Name = user.Name,
                            Surname = user.Surname,
                            Email = user.Email,
                            BirthDate = user.BirthDate,
                            RegisterDate = user.RegisterDate
                        }
                    }
                    );

            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(LogIn)}()");
                return InternalServerError();
            }
        }

        [HttpPost("logout")]
        [Authorize]
        [SwaggerResponse(StatusCodes.Status200OK, null)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> LogOut()
        {
            try
            {
                var user = await UserManager.GetUserAsync(HttpContext.User);
                if (user == null)
                    return Unauthorized();

                await TokenService.DeactivateRefreshToken(user);

                return Ok();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(LogOut)}()");
                return InternalServerError();
            }
        }

        [HttpPost("register")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, null)]
        [SwaggerResponse(StatusCodes.Status400BadRequest, typeof(Dictionary<string, string[]>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, null)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, null)]
        public async Task<IActionResult> Register(NewUserSpec request)
        {

            try
            {
                if (await UserManager.FindByEmailAsync(request.Email) != null)
                    return BadRequest(ToErrorDict("Email", "This email is already taken"));

                Data.Tables.House house = null;

                if (request.HouseId.HasValue)
                {
                    house = Context.Houses.FirstOrDefault(i => i.Id == request.HouseId.Value);

                    if (house == null)
                        return NotFound(ToErrorDict("HouseId", "This house doesn't exist"));
                }
                else
                {
                    if (string.IsNullOrEmpty(request.HouseAddress))
                        return BadRequest(ToErrorDict("HouseAddress", "This field is required"));

                    if (string.IsNullOrEmpty(request.HouseName))
                        return BadRequest(ToErrorDict("HouseName", "This field is required"));

                    house = new Data.Tables.House();
                    house.Address = request.HouseAddress;
                    house.Name = request.HouseName;

                    Context.Houses.Add(house);
                    Context.SaveChanges();
                }

                var applicationUser = new ApplicationUser()
                {
                    UserName = request.Email,
                    Email = request.Email,
                    Name = request.Name,
                    Surname = request.Surname,
                    RegisterDate = DateTime.Now,
                    HouseId = house != null ? (int?)house.Id : null
                };

                var result = await UserManager.CreateAsync(applicationUser, request.Password);

                if (result.Succeeded)
                    return Ok();
                else
                {
                    if (house != null)
                    {
                        Context.Houses.Remove(house);
                        Context.SaveChanges();
                    }
                }

                return BadRequest(ToErrorDict("error", "could not save user"));
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(Register)}()");
                return InternalServerError();
            }
        }
    }
}