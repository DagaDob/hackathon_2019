﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ShoppingListPro.Data;
using ShoppingListPro.Data.Tables;
using ShoppingListPro.ViewModels.House;
using ShoppingListPro.ViewModels.ShoppingList;
using ShoppingListPro.ViewModels.UserVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Services
{
    public class HouseService : IHouseService
    {
        public HouseService(SystemDbContext context, ILogger<HouseService> logger)
        {
            Context = context;
            Logger = logger;
        }

        private SystemDbContext Context { get; }
        private ILogger<HouseService> Logger { get; }


        public async Task<int> GetPageCount(int pageSize)
        {
            try
            {
                int count = await Context.CountSoftDeletable<House>();

                return (count / pageSize);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(GetPageCount)}()");
                throw;
            }
        }

        public async Task<HouseDetailsVM> GetHouse(ApplicationUser loggedUser)
        {
            try
            {
                var houseModel = await Context.Houses
                    .Include(i => i.Users)
                    .Include(i => i.ShoppingListItem)
                    .FirstOrDefaultAsync(i => i.Id == loggedUser.HouseId);

                if (houseModel == null)
                    return null;

                var house = new HouseDetailsVM();

                house.Id = houseModel.Id;
                house.Name = houseModel.Name;
                house.Address = houseModel.Address;
                house.Users = houseModel.Users
                    .Select(i => new UserShortDetailsVM()
                    {
                        Name = i.Name,
                        Surname = i.Surname
                    })
                    .ToList();

                house.ShoppingList = houseModel.ShoppingListItem
                    .Select(i => new ShoppingListItemVM()
                    {
                        ListName = i.Name,
                        Name = i.Product.Name,
                        Amount = i.Amount,
                        Unit = i.Unit
                    })
                    .ToList();

                return house;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(GetHouse)}()");
                throw;
            }
        }

        public async Task<List<HouseShortDetailsVM>> GetHouses(int page, int pageSize)
        {
            try
            {
                return await Context.Houses
                    .Where(i => !i.IsDeleted)
                    .OrderBy(i => i.Name)
                    .Skip(page * pageSize)
                    .Take(pageSize)
                    .Select(row =>
                        new HouseShortDetailsVM()
                        {
                            Id = row.Id,
                            Name = row.Name,
                            Address = row.Address
                        })
                    .ToListAsync();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(GetHouses)}()");
                throw;
            }
        }

        public async Task<bool> EditHouse(EditHouseSpec request, ApplicationUser loggedUser)
        {
            try
            {
                var houseToEdit = await Context.Houses
                    .FirstOrDefaultAsync(i => !i.IsDeleted && i.Id == loggedUser.HouseId);

                if (houseToEdit == null)
                    return false;

                houseToEdit.Name = request.Name;
                houseToEdit.Address = request.Address;

                await Context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(EditHouse)}()");
                throw;
            }
        }
    }
}
