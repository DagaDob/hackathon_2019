﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ShoppingListPro.Data;
using ShoppingListPro.Data.Tables;
using ShoppingListPro.ViewModels.ShoppingList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Services
{
    public class ShoppingService : IShoppingService
    {
        public ShoppingService(SystemDbContext context, ILogger<ShoppingService> logger)
        {
            Context = context;
            Logger = logger;
        }

        private SystemDbContext Context { get; }
        private ILogger<ShoppingService> Logger { get; }



        public async Task<int> GetPageCount(int pageSize)
        {
            try
            {
                int count = await Context.CountSoftDeletable<ShoppingListItem>();

                return (count / pageSize);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(GetPageCount)}()");
                throw;
            }
        }

        public async Task<List<ShoppingItemVM>> GetShoppingList(ApplicationUser loggedUser, int page, int pageSize)
        {
            try
            {
                var result = await Context.ShoppingListItems
                    .Include(i => i.Creator)
                    .Include(i => i.Product)
                    .Where(i => !i.IsDeleted && i.HouseId == loggedUser.HouseId.Value)
                    .OrderBy(i => i.Product.Cathegory)
                    .Skip(page * pageSize)
                    .Take(pageSize)
                    .Select(i => new ShoppingItemVM()
                    {
                        Id = i.Id,
                        ListName = i.Name,
                        Name = i.Product.Name,
                        Amount = i.Amount,
                        CreatorId = i.Creator.Id,
                        CreatorName = i.Creator.Name,
                        HouseId = i.HouseId,
                        ShopAddress = i.Product.ShopAddress,
                        Unit = i.Unit,
                        Category = i.Product.Cathegory
                    }).ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(GetShoppingList)}()");
                throw;
            }
        }

        public async Task<bool> DeleteShoppingListItem(int id, ApplicationUser loggedUser)
        {
            try
            {
                var itemToDelete = await Context.ShoppingListItems
                    .FirstOrDefaultAsync(i => i.Id == id && i.HouseId == loggedUser.HouseId.Value && !i.IsDeleted);

                if (itemToDelete == null)
                    return false;

                itemToDelete.IsDeleted = true;
                Context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(DeleteShoppingListItem)}()");
                throw;
            }
        }

        public async Task<bool> AddOrUpdateShoppingListItem(ShoppingItemSpec request, ApplicationUser loggedUser)
        {
            try
            {
                ShoppingListItem item = null;

                if (request.Id.HasValue)
                {
                    item = await Context.ShoppingListItems
                        .FirstOrDefaultAsync(i => i.Id == request.Id.Value && i.HouseId == loggedUser.HouseId.Value && !i.IsDeleted);

                    if (item == null)
                        return false;
                }
                else
                {
                    item = new ShoppingListItem();

                    await Context.ShoppingListItems.AddAsync(item);

                    item.UserId = loggedUser.Id;
                    item.HouseId = loggedUser.HouseId.Value;
                }

                item.Amount = request.Amount;
                item.Unit = request.Unit;
                item.ProductId = request.ProductId;
                item.Name = request.Name;

                await Context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(DeleteShoppingListItem)}()");
                throw;
            }
        }
    }
}
