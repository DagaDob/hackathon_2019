﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ShoppingListPro.Data;
using ShoppingListPro.Data.Tables;
using ShoppingListPro.ViewModels.ProductVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Services
{
    public class ProductService : IProductService
    {
        public ProductService(SystemDbContext context, ILogger<ProductService> logger)
        {
            Context = context;
            Logger = logger;
        }

        private SystemDbContext Context { get; }
        private ILogger<ProductService> Logger { get; }



        public async Task<int> GetPageCount(int pageSize)
        {
            try
            {
                int count = await Context.CountSoftDeletable<Product>();

                return (count / pageSize);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(GetPageCount)}()");
                throw;
            }
        }

        public async Task<List<ProductListItemVM>> GetProducts(ApplicationUser loggedUser, int page, int pageSize)
        {
            try
            {
                return await Context.Products
                    .Where(i => !i.IsDeleted && i.HouseId == null || i.HouseId == loggedUser.HouseId)
                    .OrderBy(i => i.Cathegory)
                    .Skip(page * pageSize)
                    .Take(pageSize)
                    .Select(row =>
                        new ProductListItemVM()
                        {
                            Id = row.Id,
                            Name = row.Name,
                            Cathegory = row.Cathegory,
                            ImageUrl = row.ImageUrl,
                            ShopAddress = row.ShopAddress,
                            HouseName = row.House.Name
                        })
                    .ToListAsync();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(GetProducts)}()");
                throw;
            }
        }

        public async Task<int?> AddProduct(CreateProductSpec request)
        {
            try
            {
                var isProductExist = await Context.Products.AnyAsync(i => i.Name.ToLower() == request.Name.ToLower());
                if (!isProductExist)
                    return null;

                var product = new Product()
                {
                    Name = request.Name,
                    Cathegory = request.Cathegory,
                    ImageUrl = request.ImageUrl,
                    ShopAddress = request.ShopAddress,
                    HouseId = request.HouseId
                };

                await Context.Products.AddAsync(product);

                await Context.SaveChangesAsync();

                return product.Id;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(AddProduct)}()");
                throw;
            }
        }

        public async Task<bool> EditProduct(EditProductSpec request, ApplicationUser loggedUser)
        {
            try
            {
                var isProductEditable = await Context.Products.AnyAsync(i => i.Id == request.Id && i.HouseId == loggedUser.HouseId);

                if (!isProductEditable)
                    return false;

                var productToEdit = await Context.Products
                    .FirstOrDefaultAsync(i => !i.IsDeleted && i.Id == request.Id);

                if (productToEdit == null)
                    return false;

                productToEdit.Name = request.Name;
                productToEdit.Cathegory = request.Cathegory;
                productToEdit.ImageUrl = request.ImageUrl;
                productToEdit.ShopAddress = request.ShopAddress;

                await Context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(EditProduct)}()");
                throw;
            }
        }
    }
}
