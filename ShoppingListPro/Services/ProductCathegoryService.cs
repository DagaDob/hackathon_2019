﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ShoppingListPro.Data;
using ShoppingListPro.Data.Tables;
using ShoppingListPro.ViewModels.ProductCathegoriesVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingListPro.Services
{
    public class ProductCathegoryService : IProductCathegoryService
    {
        public ProductCathegoryService(SystemDbContext context, ILogger<ProductCathegoryService> logger)
        {
            Context = context;
            Logger = logger;
        }

        private SystemDbContext Context { get; }
        private ILogger<ProductCathegoryService> Logger { get; }



        public async Task<int> GetPageCount(int pageSize)
        {
            try
            {
                int count = await Context.CountSoftDeletable<ProductCathegory>();

                return (count / pageSize);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(GetPageCount)}()");
                throw;
            }
        }

        public async Task<List<CathegoryVM>> GetCathegories(int page, int pageSize)
        {
            try
            {
                return await Context.ProductCathegories
                    .Where(i => !i.IsDeleted)
                    .OrderBy(i => i.Name)
                    .Skip(page * pageSize)
                    .Take(pageSize)
                    .Select(row =>
                        new CathegoryVM()
                        {
                            Id = row.Id,
                            Name = row.Name
                        })
                    .ToListAsync();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(GetCathegories)}()");
                throw;
            }
        }

        public async Task<CathegoryVM> GetCathegoryType(int id)
        {
            try
            {
                var cathegory = await Context.ProductCathegories
                    .FirstOrDefaultAsync(i => !i.IsDeleted && i.Id == id);
                
                return new CathegoryVM()
                {
                    Id = cathegory.Id,
                    Name = cathegory.Name
                };
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(GetCathegoryType)}()");
                throw;
            }
        }

        public async Task<int> AddCathegoryType(CreateCathegorySpec request)
        {
            try
            {
                var existingType = Context.ProductCathegories.FirstOrDefault(i => !i.IsDeleted && i.Name.ToLower() == request.Name.ToLower());
                if (existingType != null)
                    return existingType.Id;

                var newCathegory = new ProductCathegory()
                {
                    Name = request.Name
                };

                await Context.ProductCathegories.AddAsync(newCathegory);

                await Context.SaveChangesAsync();

                return newCathegory.Id;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"ERROR! Unexpected error in {nameof(AddCathegoryType)}()");
                throw;
            }
        }
    }
}
