﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShoppingListPro.Data;
using ShoppingListPro.ViewModels.ProductVM;

namespace ShoppingListPro.Services
{
    public interface IProductService
    {
        Task<int?> AddProduct(CreateProductSpec request);
        Task<bool> EditProduct(EditProductSpec request, ApplicationUser loggedUser);
        Task<int> GetPageCount(int pageSize);
        Task<List<ProductListItemVM>> GetProducts(ApplicationUser loggedUser, int page, int pageSize);
    }
}