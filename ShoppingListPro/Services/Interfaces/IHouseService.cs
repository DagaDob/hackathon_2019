﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShoppingListPro.Data;
using ShoppingListPro.ViewModels.House;

namespace ShoppingListPro.Services
{
    public interface IHouseService
    {
        Task<bool> EditHouse(EditHouseSpec request, ApplicationUser loggedUser);
        Task<HouseDetailsVM> GetHouse(ApplicationUser loggedUser);
        Task<List<HouseShortDetailsVM>> GetHouses(int page, int pageSize);
        Task<int> GetPageCount(int pageSize);
    }
}