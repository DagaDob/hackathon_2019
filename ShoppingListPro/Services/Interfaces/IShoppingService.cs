﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShoppingListPro.Data;
using ShoppingListPro.ViewModels.ShoppingList;

namespace ShoppingListPro.Services
{
    public interface IShoppingService
    {
        Task<bool> AddOrUpdateShoppingListItem(ShoppingItemSpec request, ApplicationUser loggedUser);
        Task<bool> DeleteShoppingListItem(int id, ApplicationUser loggedUser);
        Task<int> GetPageCount(int pageSize);
        Task<List<ShoppingItemVM>> GetShoppingList(ApplicationUser loggedUser, int page, int pageSize);
    }
}