﻿using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using ShoppingListPro.Data;

namespace ShoppingListPro.Services
{
    public interface ITokenService
    {
        Task DeactivateRefreshToken(ApplicationUser user);
        JwtSecurityToken GenerateJwtToken(string userId, string userName);
        string GenerateRefreshToken();
        Task<JwtSecurityToken> RefreshJwtToken(string refreshToken, ApplicationUser user);
    }
}