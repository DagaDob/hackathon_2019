﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShoppingListPro.ViewModels.ProductCathegoriesVM;

namespace ShoppingListPro.Services
{
    public interface IProductCathegoryService
    {
        Task<int> AddCathegoryType(CreateCathegorySpec request);
        Task<List<CathegoryVM>> GetCathegories(int page, int pageSize);
        Task<CathegoryVM> GetCathegoryType(int id);
        Task<int> GetPageCount(int pageSize);
    }
}