﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using ShoppingListPro.Data;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingListPro.Services
{
    public class TokenService : ITokenService
    {
        private IConfiguration Configuration { get; }
        private UserManager<ApplicationUser> UserManager { get; }
        private ILogger<TokenService> Logger { get; }

        public TokenService(
            IConfiguration configuration,
            UserManager<ApplicationUser> userManager,
            ILogger<TokenService> logger)
        {
            Configuration = configuration;
            UserManager = userManager;
            Logger = logger;
        }

        public string GenerateRefreshToken()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }

        public JwtSecurityToken GenerateJwtToken(string userId, string userName)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, userName),
                new Claim(ClaimTypes.NameIdentifier, userId)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtSecurityKey"]));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            
            return new JwtSecurityToken(
                //issuer: Configuration["Domain"],
                //audience: "yourdomain.com",
                claims: claims,
                expires: DateTime.Now.AddMinutes(60),
                signingCredentials: creds);
        }

        public async Task<JwtSecurityToken> RefreshJwtToken(string refreshToken, ApplicationUser user)
        {
            if (user.RefreshToken != refreshToken)
                return null;

            return GenerateJwtToken(user.Id, user.Email);
        }

        public async Task DeactivateRefreshToken(ApplicationUser user)
        {
            user.RefreshToken = null;
            var result = await UserManager.UpdateAsync(user);
        }
    }
}
