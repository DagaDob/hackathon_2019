﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NJsonSchema;
using NSwag.AspNetCore;
using NSwag.SwaggerGeneration.Processors.Security;
using ShoppingListPro.Data;
using ShoppingListPro.Data.Seeders;
using ShoppingListPro.Services;

namespace ShoppingListPro
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<Data.SystemDbContext>(options => 
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddDefaultIdentity<ApplicationUser>().AddEntityFrameworkStores<Data.SystemDbContext>();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 4;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 1;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                options.ClaimsIdentity.UserIdClaimType = System.Security.Claims.ClaimTypes.NameIdentifier;

                options.User.RequireUniqueEmail = true;
            });

            services.Configure<DataProtectionTokenProviderOptions>(options =>
            {
                options.TokenLifespan = TimeSpan.FromDays(1);
            });

            services.AddCors();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ClockSkew = TimeSpan.FromSeconds(0),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        //ValidIssuer = Configuration["Domain"],
                        //ValidAudience = "yourdomain.com",
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(Configuration["JwtSecurityKey"]))
                    };
                });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwagger();

            services.AddScoped<IHouseService, HouseService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IShoppingService, ShoppingService>();
            services.AddScoped<IProductCathegoryService, ProductCathegoryService>(); 

            services.AddScoped<AppUserSeeder>();
            services.AddScoped<PasswordHasher<ApplicationUser>>();
            services.AddScoped<HouseSeeder>();
            services.AddScoped<ProductSeeder>();
            services.AddScoped<ProductCathegorySeeder>();
            services.AddScoped<ShoppingListSeeder>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (!env.IsDevelopment())
                app.UseHsts();

            app.UseAuthentication();

            // Register the Swagger generator and the Swagger UI middlewares
            app.UseSwaggerUi3WithApiExplorer(settings =>
            {
                settings.PostProcess = document =>
                {
                    document.Schemes.Clear();
                    document.Schemes.Add(NSwag.SwaggerSchema.Https);
                    document.Schemes.Add(NSwag.SwaggerSchema.Http);
                    document.Info.Version = "v1";
                    document.Info.Title = "Shopping List Pro API";
                    document.Info.Description = "Created by Dagmara Dobrowolska";
                    //document.Info.TermsOfService = "None";

                    document.Info.Contact = new NSwag.SwaggerContact
                    {
                        Name = "Dagmara Dobrowolska",
                        Email = "dagrafa@gmail.com"
                    };
                    //document.Info.License = new NSwag.SwaggerLicense
                    //{
                    //    Name = "Use under LICX",
                    //    Url = "https://example.com/license"
                    //};
                };
                settings.GeneratorSettings.DefaultEnumHandling = EnumHandling.CamelCaseString;
                settings.GeneratorSettings.DocumentProcessors.Add(new SecurityDefinitionAppender("jwt", new NSwag.SwaggerSecurityScheme()
                {
                    Type = NSwag.SwaggerSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    Description = "JWT Token. Example: \"Value: Bearer {token}\"",
                    In = NSwag.SwaggerSecurityApiKeyLocation.Header
                }));
                settings.GeneratorSettings.OperationProcessors.Add(new OperationSecurityScopeProcessor("jwt"));
            });

            app.UseHttpsRedirection();

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            app.UseMvc();

            using (var scope = app.ApplicationServices.CreateScope())
            {
                var houseSeeder = scope.ServiceProvider.GetRequiredService<HouseSeeder>();
                houseSeeder.Seed();
                var appUserSeeder = scope.ServiceProvider.GetRequiredService<AppUserSeeder>();
                appUserSeeder.Seed();
                var productCathegorySeeder = scope.ServiceProvider.GetRequiredService<ProductCathegorySeeder>();
                productCathegorySeeder.Seed();
                var productSeeder = scope.ServiceProvider.GetRequiredService<ProductSeeder>();
                productSeeder.Seed();
                var shoppingListSeeder = scope.ServiceProvider.GetRequiredService<ShoppingListSeeder>();
                shoppingListSeeder.Seed();
            }
        }
    }
}
